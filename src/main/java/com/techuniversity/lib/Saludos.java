package com.techuniversity.lib;

/**
 * Classe para devolver saludos
 *
 */

public class Saludos {

    /**
     * Devuele un saludo formal en base al nombre que le pases
     *
     * @param nombre  Es el nombre de la persona que queremos saludar
     */
    public String getSaludoCordial(String nombre) {
        return String.format("Bienvenido %s", nombre);
    }

    public String getSaludoInformal(String nombre) {
        return String.format("Hey %s. ¿Qué pasa tio?", nombre);
    }
}
